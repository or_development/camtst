import XCTest

import HLTCamTests

var tests = [XCTestCaseEntry]()
tests += HLTCamTests.allTests()
XCTMain(tests)
